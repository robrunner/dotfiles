#!/bin/bash

ssid=$(nmcli -t -f ssid dev wifi)
if [ $ssid != "NETGEAR" ]; then
	exit 0
fi

curl --user kodi:Ni3P5%Wp --header 'Content-Type: application/json' \
	--data-binary '{ "id": 2, "jsonrpc": "2.0", "method": "Application.GetProperties", "params": { "properties": ["volume"] } }' \
	'http://htpc:8080/jsonrpc' | sed -e 's/[{}]/''/g' | awk -v k="text" '{n=split($0,a,","); for (i=1; i<=n; i++) print a[i]}' | grep volume | cut -d: -f3
