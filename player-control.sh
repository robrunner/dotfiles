#!/bin/bash

ssid=$(nmcli -t -f ssid dev wifi)
if [ $ssid == "NETGEAR" ]; then
	ping -c 1 -W 1 htpc > /dev/null
	kodicon=$?
else
	kodicon=1
fi
kodiid=1

if [[ $kodicon == 0 ]]
then
	playerid=$(curl --user kodi:Ni3P5%Wp --header 'Content-Type: application/json' \
		--data-binary '{ "id": 1, "jsonrpc": "2.0", "method": "Player.GetActivePlayers", "params": {} }' \
		'http://htpc:8080/jsonrpc' \
		| sed -e 's/[{}]/''/g' | awk -v k="text" '{n=split($0,a,","); for (i=1; i<=n; i++) print a[i]}' | grep result)

	kodiid="${playerid:$((${#playerid}-1)):1}"
fi

cmusid=$(pidof cmus)

if [[ $kodiid == 0 ]]
then
	if [[ $1 == "pause" ]]
	then
		curl --user kodi:Ni3P5%Wp --header 'Content-Type: application/json' \
			--data-binary '{ "id": 1, "jsonrpc": "2.0", "method": "Player.PlayPause", "params": { "playerid": 0 } }' \
			'http://htpc:8080/jsonrpc'
	elif [[ $1 == "next" ]]
	then
		curl --user kodi:Ni3P5%Wp --header 'Content-Type: application/json' \
			--data-binary '{ "id": 1, "jsonrpc": "2.0", "method": "Player.GoTo", "params": { "playerid": 0, "to": "next" } }' \
			'http://htpc:8080/jsonrpc'
	elif [[ $1 == "prev" ]]
	then
		curl --user kodi:Ni3P5%Wp --header 'Content-Type: application/json' \
			--data-binary '{ "id": 1, "jsonrpc": "2.0", "method": "Player.GoTo", "params": { "playerid": 0, "to": "previous" } }' \
			'http://htpc:8080/jsonrpc'
	elif [[ $1 == "volup" ]]
	then
		curl --user kodi:Ni3P5%Wp --header 'Content-Type: application/json' \
			--data-binary '{ "id": 1, "jsonrpc": "2.0", "method": "Application.SetVolume", "params": { "volume": "increment" } }' \
			'http://htpc:8080/jsonrpc'
	elif [[ $1 == "voldown" ]]
	then
		curl --user kodi:Ni3P5%Wp --header 'Content-Type: application/json' \
			--data-binary '{ "id": 1, "jsonrpc": "2.0", "method": "Application.SetVolume", "params": { "volume": "decrement" } }' \
			'http://htpc:8080/jsonrpc'
	fi
else 
	if [[ $cmusid > 0 ]]
	then
		if [[ $1 == "pause" ]]
		then
			cmus-remote -u
		elif [[ $1 == "next" ]]
		then
			cmus-remote -n
		elif [[ $1 == "prev" ]]
		then
			cmus-remote -p
		elif [[ $1 == "volup" ]]
		then
			cmus-remote -v +5%
		elif [[ $1 == "voldown" ]]
		then
			cmus-remote -v -5%
		fi
	fi
fi
