#!/bin/bash

pid=$(pidof conky)

if [ $pid -gt 0 ]; then
	kill $pid
fi

conky &
